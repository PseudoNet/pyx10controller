# Copyright (c) 2011 by Kenneth Dixon
#
# GNU General Public Licence (GPL)
# 
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#

#Global
LOGGER_NAME='X10Client'
LOG_FILE_NAME='X10Client.log'

#Connection Details
HOST="localhost"
PORT=1099

#makes up the recieve string to look for
REC_PREFIX = "Rx RF HouseUnit: "
REC_MOTION_SENSOR_1 = "C1"
REC_LIGHT_SENSOR_1 = "C2"
REC_MOTION_SENSOR_2 = "C3"
REC_LIGHT_SENSOR_2 = "C4"
REC_POSTFIX_ON = " Func: On"
REC_POSTFIX_OFF = " Func: Off"

#makes up the send string
SEND_PREFIX = "pl "
SEND_LIGHT_1 = "a1"
SEND_LIGHT_2 = "a2"
SEND_POSTFIX_ON = " on\n"
SEND_POSTFIX_OFF = " off\n"

#Light Dark variable
isDark = True

#Configuration
WORKING_DIR = "/home/i/.PyX10Controller/"

#IP Camera Config
CAM_IP = "192.168.1.125"
CAM_PORT = "8080"
CAM_USER = "<IP Camera User>"
CAM_PASSWORD = "<IP Camera password>"
CAM_RES = "32" #640x480
CAM_SNAPSHOT_PAGE = "/snapshot.cgi"
CAM_SNAPSHOT_ROOT_DIR = "./snapshots"
CAM_SNAPSHOTS_NUMBER = 5
CAM_SNAPSHOTS_DELAY_SEC = 1

#Email Config
EMAIL_SERVER_SMTP="smtp.gmail.com"
EMAIL_SERVER_PORT=587
EMAIL_FROM = "<Sending Email Address>"
EMAIL_TO = "<Revieving Email Address>"
EMAIL_PASSWORD = "<Sending Email Address Password>"
EMAIL_SUBJECT = "Motion Sensor Alert"
EMAIL_EXTERNAL_IP_URL = "http://automation.whatismyip.com/n09230945.asp"
EMAIL_HTML_MESSAGE_1 = """\
<html>
  <head><blink><font size="10" color="red">ALERT!!!</font></blink></head>
  <body>
    <p>A motion alert has been detected!<br>
       Attached are some images of the suspect<br>
       Here is a link to the camera: <a href="http://
"""
EMAIL_HTML_MESSAGE_2 = """\
    ">IP Cam</a>
    </p>
  </body>
</html>
"""




